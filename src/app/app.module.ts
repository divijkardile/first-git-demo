import { DashboardComponent } from './Components/dashboard/dashboard.component';
import { DeletedetailsComponent } from './Components/deletedetails/deletedetails.component';
import { DetailsComponent } from './Components/details/details.component';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';


import { UpdateComponent } from './Components/update/update.component';
import { AddDetailsComponent } from './add-details/add-details.component';


@NgModule({
  declarations: [
    AppComponent,
    DetailsComponent,
    DeletedetailsComponent,
    UpdateComponent,
    DashboardComponent,
    AddDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]

})
export class AppModule {}
